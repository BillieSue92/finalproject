from numpy import sin, cos, pi, array, arctan, sqrt
import random
import math


class Model:
	def __init__(self):
		# geometric parameters
		self.canvas = (600, 300)

		# ball
		self.ball = Circle([self.canvas[0]/2,self.canvas[1]/2], 5, 4)

		# paddles
		self.paddleDimensions = (10, 50)
		self.leftPaddle = Obstacle((2, self.canvas[1]/2-self.paddleDimensions[1]/2), self.paddleDimensions, "left paddle")
		self.rightPaddle = Obstacle((self.canvas[0]-self.paddleDimensions[0], self.canvas[1]/2-self.paddleDimensions[1]/2), self.paddleDimensions, "right paddle")

		# score card
		self.score = [0,0]

		# hit trackers
		self.hitCount = 1

		# obstacle
		self.obstacleList = []	# list of objects from the Obstacle class

		self.level = 0

		self.quit = False
	
	# object functions
	def getBall(self):
		return self.ball

	def getLeftPaddle(self):
		return self.leftPaddle
		# this function returns the left paddle "object" -> see Obstacle class below
		# to modify this, you must treat it like an object within an object
		# example:
		# A = Model()
		# to get the position of the left paddle within the A model, you must call
		# A.getLeftPaddle().getPosition()
		# or you can also do
		# B = A.getLeftPaddle()
		# B.getPosition()

	def getRightPaddle(self):
		return self.rightPaddle

	def newBall(self):
		self.ball = Circle([self.canvas[0]/2,self.canvas[1]/2], 5, 4)

	def newPaddles(self):
		self.leftPaddle = Obstacle(self.leftPaddle.getPosition(), self.paddleDimensions, "left paddle")
		self.rightPaddle = Obstacle(self.rightPaddle.getPosition(), self.paddleDimensions, "right paddle")

	def setPaddles(self, left, right):
		self.leftPaddle = left
		self.rightPaddle = right

	def deleteList(self):
		self.obstacleList = []

	# status funcitons
	def getCanvasSize(self):
		return self.canvas

	def getScore(self):
		return self.score

	def getHitCount(self):
		return self.hitCount

	def getObstacleList(self):
		return self.obstacleList
		# this function returns a list of obstacle objects
		# to access the objects in this list you must call
		# A = Model()
		# A.getObstacles()[0]
		# to modify the obstacles in this list you must call
		# B = A.getObstacles()[0]
		# B.getPosition()
		# or
		# A.getObstacles()[0].getPosition()

	# def getCircleList(self):
	# 	return self.circleList

	def getLevel(self):
		return self.level

	def increaseScore(self, score):
		self.score[0] = self.score[0] + score[0]
		self.score[1] = self.score[1] + score[1]

	def increaseHitCount(self):
		self.hitCount += 1

	def resetHitCount(self):
		self.hitCount = 1

	def newLevel(self):
		# self.level = random.randint(0,8)
		self.level += 1

	def setLevel(self, l):
		self.level = l

	# def addObstacle(self, obstacle):
	# 	self.obstacleList.append(obstacle)
	# 	# this adds an obstacle to the obstacleList
	# 	# input MUST be an obstacle object

	# def removeObstale(self, obstacle):
	# 	self.obstacleList.remove(obstacle)
	# 	# code not tested

	def quit(self):
		return self.quit
	
	def switchQuit(self):
		self.quit = not self.quit

class Obstacle:
	# obstacles are generally stagnant except for paddles
	def __init__(self, position = [0, 0], dimensions = [1,1], name =  "Obstacle"):
		self.position = position
		self.previous = self.position
		self.dimensions = dimensions
		self.speed = 5
		self.name = name
		self.hit = -1 # hit boolean for one-time-hit obstacles
		self.type = 1

	def getType(self):
		return self.type

	def printType(self):
		print "Obstacle:", self.name

	def getPosition(self):
		return self.position

	def getDimensions(self):
		return self.dimensions

	def getDelta(self):
		dx = self.position[0] - self.previous[0]
		dy = self.position[1] - self.previous[1]
		return [dx, dy]

	def getPrevious(self):
		return self.previous

	def getSpeed(self):
		return self.speed

	def setDimensions(self, dimensions):
		self.dimensions = dimensions
	
	def setPosition(self, p):
		self.previous = self.position
		self.position = p

	def movePosition(self, delta):
		# delta = (deltaX, deltaY)
		# positive x goes right, positive y goes up
		self.previous = self.position
		self.position = [self.position[0] + delta[0], self.position[1] + delta[1]]

	def increaseSpeed(self, ds):
		self.speed += ds
	
	def isHit(self):
		if (self.hit > 0):
			return True
		else:
			return False

	def switchHit(self):
		# reverses hit boolean
		self.hit = -1 * self.hit

class Circle (Obstacle):
	# balls are able to move continuously around the screen
	def __init__(self, position = [0,0], radius = 5, speed = 4, name = "ball"):
		self.position = position
		self.dimensions = radius 
		self.previous = self.position			# previous position of ball

		self.speed = speed

		self.theta = random.randint(0,360)*pi/180	# initial direction of ball
		while(self.theta == 90*pi/180 or self.theta == 270*pi/180):
		 	self.theta = random.randint(0,360)*pi/180
		self.velocity = [self.speed*cos(self.theta), self.speed*sin(self.theta)]
		self.gravity = 0

		self.name = name

		self.isObstacle = False
		self.isCircle = True
		self.hit = -1
		self.type = -1

	def printType(self):
		print "Ball:", self.name

	def getTheta(self):
		return self.theta*180/pi
	
	def getVelocity(self):
		return self.velocity

	def getGravity(self):
		return float(self.gravity)

	def newTheta(self):
		# creates new random angle
		self.theta = random.randint(0,360)*pi/180
		while(self.theta == 90*pi/180 or self.theta == 270*pi/180):
		 	self.theta = random.randint(0,360)*pi/180
		self.velocity = [self.speed*cos(self.theta), self.speed*sin(self.theta)]

	def setTheta(self, t):
		# sets theta at desired angle
		if (t != 90 and t != 270):
			self.theta = t*pi/180	
			self.velocity = [self.speed*cos(self.theta), self.speed*sin(self.theta)]
	
	def setVelocity(self, v):
		self.velocity = v
		self.theta = arctan(v[1]/v[0])
		self.speed = sqrt(v[0]**2 + v[1]**2)

	def setSpeed(self, s):
		self.speed = s
		self.velocity = [self.speed*cos(self.theta), self.speed*sin(self.theta)]

	def increaseSpeed(self, ds):
		self.speed += ds
		self.velocity = [self.speed*cos(self.theta), self.speed*sin(self.theta)]

	def setGravity(self, gravity):
		self.gravity = gravity

	def increaseGravity(self, dg):
		self.gravity += dg
