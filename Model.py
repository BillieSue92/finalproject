from numpy import sin, cos, pi, array, arctan, sqrt
import random
import math


class Model:
	def __init__(self):
		# geometric parameters
		self.canvas = (600, 300)
		
		# ball
		self.ball = Ball([self.canvas[0]/2,self.canvas[1]/2], 5, 4)

		# paddles
		self.paddleDimensions = (10, 50)
		self.leftPaddle = Obstacle((2, self.canvas[1]/2-self.paddleDimensions[1]/2), self.paddleDimensions)
		self.rightPaddle = Obstacle((self.canvas[0]-self.paddleDimensions[0], self.canvas[1]/2-self.paddleDimensions[1]/2), self.paddleDimensions)

		# score card
		self.score = [0,0]

		# hit trackers
		self.hitMax = 5 + 1
		self.hitCount = 1

		# obstacle
		self.obstacleList =[]	# list of objects from the Obstacle class
	
	# object functions
	def getBall(self):
		return self.ball

	def getLeftPaddle(self):
		return self.leftPaddle
		# this function returns the left paddle "object" -> see Obstacle class below
		# to modify this, you must treat it like an object within an object
		# example:
		# A = Model()
		# to get the position of the left paddle within the A model, you must call
		# A.getLeftPaddle().getPosition()
		# or you can also do
		# B = A.getLeftPaddle()
		# B.getPosition()

	def getRightPaddle(self):
		return self.rightPaddle

	# status funcitons
	def getCanvasSize(self):
		return self.canvas

	def getScore(self):
		return self.score

	def getHitMax(self):
		return self.hitMax

	def getHitCount(self):
		return self.hitCount

	def getObstacles(self):
		return self.obstacleList
		# this function returns a list of obstacle objects
		# to access the objects in this list you must call
		# A = Model()
		# A.getObstacles()[0]
		# to modify the obstacles in this list you must call
		# B = A.getObstacles()[0]
		# B.getPosition()
		# or
		# A.getObstacles()[0].getPosition()

	def increaseScore(self, score):
		self.score[0] = self.score[0] + score[0]
		self.score[1] = self.score[1] + score[1]

	def increaseHit(self):
		self.hitCount += 1

	def addObstacle(self, obstacle):
		self.obstacleList.append(obstacle)
		# this adds an obstacle to the obstacleList
		# input MUST be an obstacle object

class Obstacle:
	def __init__(self, position = [0, 0], dimensions = [1,1]):
		self.position = position
		self.dimensions = dimensions

	def getPosition(self):
		return self.position

	def getDimensions(self):
		return self.dimensions

	def setPosition(self, position):
		self.position = position

	def movePosition(self, delta):
		# delta = (deltaX, deltaY)
		# positive x goes right, positive y goes up
		self.position = [self.position[0] + delta[0], self.position[1] - delta[1]]

	def setDimensions(self, dimensions):
		self.dimensions = dimensions

class Ball:
	def __init__(self, position = [0,0], radius = 5, speed = 4):
		self.position = position
		self.radius = radius 
		self.previous = self.position			# previous position of ball

		self.speed = speed

		self.theta = random.randint(0,360)*pi/180	# initial direction of ball
		self.velocity = [self.speed*cos(self.theta), self.speed*sin(self.theta)]
		self.gravity = 0

	def getTheta(self):
		return self.theta

	def getRadius(self):
		return self.radius

	def getPosition(self):
		return self.position

	def getDelta(self):
		dx = self.position[0] - self.previous[0]
		dy = self.position[1] - self.previous[1]
		return [dx, dy]
	
	def getVelocity(self):
		return self.velocity

	def getSpeed(self):
		return self.speed

	def getGravity(self):
		return self.gravity

	# modification funcitons
	def newTheta(self):
		# creates new random angle
		self.theta = random.randint(0,360)*pi/180
		self.velocity = [self.speed*cos(self.theta), self.speed*sin(self.theta)]

	def setTheta(self, t):
		# sets theta at desired angle
		self.theta = t*pi/180	
		self.velocity = [self.speed*cos(self.theta), self.speed*sin(self.theta)]

	def setRadius(self, r):
		self.radius = r
	
	def setPosition(self, p):
		self.previous = self.position
		self.position = p

	def setVelocity(self, v):
		self.speed = v
		self.velocity = [self.speed*cos(self.theta), self.speed*sin(self.theta)]

	def setGravity(self, gravity):
		self.gravity = gravity


