from Tkinter import *
from View import Pong, Paddles
from ModelComponents import Model
from ModelLogic import update, goal, levelComponents
from Controller import controllerMain

model = Model()
canvasWidth, canvasHeight = model.getCanvasSize()

root = Tk()
pongFrame = Frame(root)
pongFrame.pack()
board = Canvas(root, width = canvasWidth, height = canvasHeight)
board.pack()

view = Pong(model, board)

leftPaddle = view.getPaddleItem(paddle = "left")
rightPaddle = view.getPaddleItem(paddle = "right")

controller = controllerMain(root, model, board, canvasWidth, canvasHeight, leftPaddle, rightPaddle)

def tasks():

	update(model)
	view.redraw()

	if goal(model):
		view.updateScore()
		model.resetHitCount()
		levelComponents(0, model)
		model.setLevel(7)
		model.newBall()
		levelComponents(model.getLevel(), model)
		view.resetBall()

	root.after(25,tasks)

root.after(25, tasks)
root.mainloop()
