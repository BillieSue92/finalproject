# ---------------------------------------------------- #
# File: View.py
# ---------------------------------------------------- #
# Author(s): Crystal Liu, Ivy McGinley
# BitBucket - user: BillieSue92
# ---------------------------------------------------- #
# Plaftorm:    Windows
# Environment: Python 2.7.7 :Anaconda 2.0.1
# Libaries:    Tkinter
# ---------------------------------------------------- #
# Description: Two-player game of Pong
#			   Player 1 keys: w,s
#			   Player 2 keys: o,l
#			   Quit game: q
# ---------------------------------------------------- #


#----------------------------------------------------------------#

#						IMPORTED MODULES                         #

#----------------------------------------------------------------#

from Tkinter import PhotoImage
import random
from Controller import controllerMain
from PIL import Image, ImageTk

#----------------------------------------------------------------#

#							Pong Game   - View                   #

#----------------------------------------------------------------#

# main game
class Pong:

	def __init__(self, model, canvas): 
		self.model = model
		self.canvas = canvas

		# Creating arena
		self.arena = Arena(self.model, self.canvas)

		# Creating paddles
		self.paddles = Paddles(self.model, self.canvas)

		# Creating ball
		self.ball = Ball(self.model, self.canvas)

	def redraw(self):

		# updating ball
		self.ball.moveBall()

		leftPaddle = self.paddles.getPaddle(paddle = "left")
		rightPaddle = self.paddles.getPaddle(paddle = "right")

		# creating obstacle
		self.obstacles = Obstacles(self.model, self.canvas, leftPaddle, rightPaddle)

	def resetBall(self):
		self.canvas.delete("gameBall")

		ballPosition = self.model.getBall().getPosition()
		ballRadius = self.model.getBall().getDimensions()
		ballSpeed = self.model.getBall().getSpeed()
		self.ball.drawBall()

	def getPaddleItem(self, paddle = ""):
		if paddle == "left":
			return self.paddles.getPaddle(paddle ="left")

		if paddle == "right":
			return self.paddles.getPaddle(paddle = "right")

	def getBallItem(self):
		return self.ball.getBall()

	def updateScore(self):
		self.arena.updateScore()

class Arena:

	def __init__(self, model, canvas):

		self.model = model
		self.canvas = canvas

		self.width = self.model.getCanvasSize()[0]

		self.player1Points, self.player2Points = model.getScore()

		self.TextLabel = self.canvas.create_text(self.width/2,10,
			text=str(self.player1Points)+" | "+str(self.player2Points))

	def getCanvas(self):
		return canvas

	def updateScore(self):
		self.player1Points, self.player2Points = self.model.getScore()

		self.canvas.delete(self.TextLabel)
		self.TextLabel = self.canvas.create_text(self.width/2,10,
			text=str(self.player2Points)+" | "+str(self.player1Points))

class Paddles:

	def __init__(self, model, canvas):

		self.model = model
		self.canvas = canvas
		self.drawPaddles()

	def drawPaddles(self):

		def drawRectangle(canvas, position, dimensions, color = "orange", tags = None):
			rectangle = canvas.create_rectangle(position[0], position[1], position[0] + dimensions[0],
			position[1] + dimensions[1], outline=color, fill=color, tags = tags)
			return rectangle

		# Creating left paddle
		leftPaddlePosition = self.model.getLeftPaddle().getPosition()
		leftPaddleDimensions = self.model.getLeftPaddle().getDimensions()
		self.paddleLeft = drawRectangle(self.canvas, leftPaddlePosition, leftPaddleDimensions, color = "red", tags = "paddles")

		# Creating right paddle
		rightPaddlePosition = self.model.getRightPaddle().getPosition()
		rightPaddleDimensions = self.model.getRightPaddle().getDimensions()
		self.paddleRight = drawRectangle(self.canvas, rightPaddlePosition, rightPaddleDimensions, color = "blue", tags = "paddles")

	def getPaddle(self, paddle = ""):
		if paddle == "left":
			return self.paddleLeft

		if paddle == "right":
			return self.paddleRight

	def updatePaddles(self):
		leftPaddlePosition = self.canvas.coords(self.paddleLeft)
		rightPaddlePosition = self.canvas.coords(self.paddleRight)
		self.model.getLeftPaddle().setPosition([leftPaddlePosition[0], leftPaddlePosition[1]])
		self.model.getRightPaddle().setPosition([rightPaddlePosition[0], rightPaddlePosition[1]])

class Ball:

	def __init__(self, model, canvas):

		self.model = model
		self.canvas = canvas

		self.drawBall()

	def drawBall(self):
		position = self.model.getBall().getPosition()
		radius = self.model.getBall().getDimensions()
		speed = self.model.getBall().getSpeed()
		self.gameBall = self.canvas.create_oval(position[0] - radius, position[1] - radius,
			position[0] + radius, position[1] + radius, outline = "black", fill = "green", width = 1, tags = "gameBall")

	def getBall(self):
		return self.gameBall

	def moveBall(self):

		gameBalldx, gameBalldy = self.model.getBall().getDelta()
		self.canvas.move(self.gameBall, gameBalldx, gameBalldy)

class Obstacles:

	def __init__(self, model, canvas, paddleLeft, paddleRight):

		def drawRectangle(canvas, position, dimensions, color = "orange", tags = None):
			canvas.create_rectangle(position[0], position[1], position[0] + dimensions[0],
				position[1] + dimensions[1], outline=color, fill=color, tags = tags)

		def drawCircle(canvas, position, radius, color = "green", tags = None):
			canvas.create_oval(position[0] - radius, position[1] - radius, position[0] + radius,
				position[1] + radius, outline = "black", fill = color, width = 1, tags = tags)

		def paddleSpeed():
			pass

		def portal(obstacleList, canvas):

			left, right = obstacleList

			leftPosition = left.getPosition()
			leftDimensions = left.getDimensions()

			rightPosition = right.getPosition()
			rightDimensions = right.getDimensions()

			self.leftItem = drawRectangle(canvas, leftPosition, leftDimensions, color = "black")

			self.rightItem = drawRectangle(canvas, rightPosition, rightDimensions, color = "black")

		def breakWall(obstacleList, canvas):
			listLength = len(obstacleList)
			for ii in range(0,listLength):
				function = obstacleList[ii]
				position = function.getPosition()
				dimensions = function.getDimensions()
				drawRectangle(canvas, position, dimensions, color = "cyan", tags = "breakWall")

		def wall(obstacleList, canvas):
			topWall, bottomWall = obstacleList

			topWallPosition = topWall.getPosition()
			topWallDimensions = topWall.getDimensions()

			bottomWallPosition = bottomWall.getPosition()
			bottomWallDimensions = bottomWall.getDimensions()

			self.topWallItem = drawRectangle(canvas, topWallPosition, topWallDimensions, color = "purple")

			self.bottomWallItem = drawRectangle(canvas, bottomWallPosition, bottomWallDimensions, color = "purple")

		def creature(obstacleList, canvas):
			position = obstacleList[0].getPosition()
			dimensions = obstacleList[0].getDimensions()
			self.creatureImage = PhotoImage(file= "Creature.gif")
			canvas.create_image(position[0] + dimensions[0]/2, position[1] + dimensions[1]/2,
				image=self.creatureImage, tags = "creature")

		def chaoticCloud(obstacleList, canvas):
			position = obstacleList[0].getPosition()
			dimensions = obstacleList[0].getDimensions()
			self.chaoticCloudImage = PhotoImage(file= "Cloud.gif")
			canvas.create_image(position[0] + dimensions[0]/2, position[1] + dimensions[1], image=self.chaoticCloudImage)
		
		level = model.getLevel()

		# if level == 2:
		# 	leftPaddle = model.getLeftPaddle()
		# 	rightPaddle = model.getRightPaddle()
		# 	circlePaddles(canvas, paddleLeft, paddleRight)

		if level >= 3 and level <= 7:
			list = model.getObstacleList()
			obstacleType = model.getLevel()
			listTypes = {3: portal, 4: breakWall, 5: wall, 6: creature, 7: chaoticCloud}
			func = listTypes[obstacleType]
			func(list, canvas)

