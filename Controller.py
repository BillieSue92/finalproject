from Tkinter import Frame

class controllerMain:

	def __init__(self, master, model, canvas, canvasWidth, canvasHeight, paddle1, paddle2):

		self.master = master
		self.model = model
		self.canvas = canvas

		self.WinWidth = canvasWidth
		self.WinHeight = canvasHeight
		self.Paddle1 = paddle1
		self.Paddle2 = paddle2
		self.PaddleSpeed = self.model.getLeftPaddle().getSpeed()

		self.master.bind("<Key>", self.Keys)


	def Keys(self, event):

		if event.char == 'w':
			if self.canvas.coords(self.Paddle1)[1]>=self.PaddleSpeed/2:
				self.canvas.move(self.Paddle1,0,-self.PaddleSpeed)
				self.model.getLeftPaddle().movePosition([0, -self.PaddleSpeed])

		if event.char == 's':
			if self.canvas.coords(self.Paddle1)[3]<=self.WinHeight - self.PaddleSpeed/2:
				self.canvas.move(self.Paddle1,0,self.PaddleSpeed)
				self.model.getLeftPaddle().movePosition([0, self.PaddleSpeed])

		if event.char == 'o':
			if self.canvas.coords(self.Paddle2)[1]>=self.PaddleSpeed/2:
				self.canvas.move(self.Paddle2,0,-self.PaddleSpeed)
				self.model.getRightPaddle().movePosition([0, -self.PaddleSpeed])

		if event.char == 'l':
			if self.canvas.coords(self.Paddle2)[3]<=self.WinHeight - self.PaddleSpeed/2:
				self.canvas.move(self.Paddle2,0,self.PaddleSpeed)
				self.model.getRightPaddle().movePosition([0, self.PaddleSpeed])

		# quit game			
		if event.char == 'q':
			self.master.destroy()
 