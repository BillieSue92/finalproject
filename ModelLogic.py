from numpy import sin, cos, pi, array, arctan, sqrt
import random
import math
import Tkinter

from ModelComponents import Model, Obstacle, Circle

# levels can add on top of each other
# level 0: original pong game - ball speeds up with more hits
#	can also be used to reset pong to original settings
# level 1: increase paddle speed
# level 2: paddle shape - change to semicircle
# level 3: portals
# level 4: break out
# level 5: walls
# lavel 6: creature
# level 7: chaotic cloud
# level 8: gravity field

def update(model):
	ball = model.getBall()
	moveCircle(ball)
	#check bounces off boundary
	checkBoundary(ball, model.getCanvasSize())

	if( checkObstacle(ball, model.getLeftPaddle()) or checkObstacle(ball, model.getRightPaddle()) ):
		model.increaseHitCount()
	if(model.getHitCount() % 5 == 0):
		# increase velocity after a certain amount of hits
		ball.increaseSpeed(2)
		model.increaseHitCount()
		print "SPEED UP"
		if(model.getLevel() == 1):
			# increase paddle speeds
			model.getLeftPaddle().increaseSpeed(2)
			model.getRightPaddle().increaseSpeed(2)
		if (model.getLevel() == 8):
			# increase gravity
			ball.increaseGravity(.1)

	if (model.getLevel() == 3):
		left = model.getObstacleList()[0]
		right = model.getObstacleList()[1]

		if (crossesLineVertical(ball, left.getPosition()[0] + left.getDimensions()[0]/2, [left.getPosition()[1], left.getPosition()[1] + left.getDimensions()[1]])):
			ball.setPosition([right.getPosition()[0], 65])
			ball.setPosition([right.getPosition()[0], 65])
		elif (crossesLineVertical(ball, right.getPosition()[0] + right.getDimensions()[0]/2, [right.getPosition()[1], right.getPosition()[1] + right.getDimensions()[1]])):
			ball.setPosition([left.getPosition()[0], 215])
			ball.setPosition([left.getPosition()[0], 215])

	if (model.getLevel() >= 4 and model.getLevel() <= 7):
		obstacleList = model.getObstacleList()
		for i in range(len(obstacleList)):
			obstacle = obstacleList[i]

			if (model.getLevel() == 6):
				leftWall = Obstacle([0,0] , [0, model.getCanvasSize()[1]], "left wall")
				rightWall = Obstacle([model.getCanvasSize()[0], 0] , [0, model.getCanvasSize()[1]] , "right wall")
				moveCircle(obstacle)
				checkBoundary(obstacle, model.getCanvasSize())
				checkObstacle(obstacle, left)
				checkObstacle(obstacle, right)
			if (model.getLevel() == 4):
				checkObstacle(ball, obstacle)
				if (obstacle.isHit()):
					obstacleList.remove(obstacle)
			if (model.getLevel() == 7):
				x, y = obstacle.getPosition()
				w, h = obstacle.getDimensions()

				if(crossesLineVertical(ball, x, [y, y+h]) or crossesLineVertical(ball, x+w, [y, y+h]) or crossesLineHorizontal(ball, y, [x, x+w]) or crossesLineHorizontal(ball, y + h, [x, x+w])):
					ball.newTheta()

def levelComponents(level, model):
	if (level == 0):
		model.newPaddles()
		model.deleteList()
		model.getBall().setGravity(0)

	elif(level == 2):
		newLeft = Circle(model.getLeftPaddle().getPosition(), 25, 5, "left paddle")
		newRight = Circle(model.getRightPaddle().getPosition(), 25, 5, "right paddle")

		model.setPaddles(newLeft, newRight)

	elif(level == 3):
		left = Obstacle([100, 200], [5, 60], "left obstacle")
		right = Obstacle([500, 50], [5, 60], "right obstacle")

		model.getObstacleList().append(left)
		model.getObstacleList().append(right)

	elif(level == 4):
		x = 250
		y = 100
		for i in range (0, 10):
			for j in range(0, 10):
				block = Obstacle([x, y], [10, 10], "break block")
				model.getObstacleList().append(block)
				x += 10
			y += 10

	elif(level == 5):
		topWall = Obstacle([295, 0], [10, 100], "top wall")
		bottomWall = Obstacle([295, 200], [10, 100], "bottom wall")

		model.getObstacleList().append(topWall)
		model.getObstacleList().append(bottomWall)

	elif(level == 6):
		creature = Circle([300, 150], 30, "creature")
		model.getObstacleList().append(creature)

	elif (level == 7):
		cloud = Obstacle([200, 0], [200, 200], "cloud")
		model.getObstacleList().append(cloud)	

def goal(model):
	goalScored = False

	ball = model.getBall()
	canvas = model.getCanvasSize()

	if (crossesLineVertical(ball, 0, [0, canvas[1]])):
		# left goal is scored
		model.increaseScore([1,0])
		goalScored = True
	elif(crossesLineVertical(ball, canvas[0], [0, canvas[1]])):
		# right goal is scored
		model.increaseScore([0,1])
		goalScored = True

	return goalScored

def moveCircle(ball):
	x, y = ball.getPosition()
	dx, dy = ball.getVelocity()

	dy += ball.getGravity()
	x += dx
	y += dy

	ball.setVelocity([dx, dy])
	ball.setPosition([x, y])

# if ball is out of bounds, repositions ball on trajectory within bounds
def repositionx(ball, newY):
	# repositions x based on a known y
	
	x,y = ball.getPosition()
	dx, dy = ball.getVelocity()
	x = x - (dx/dy)*(y - newY)
	y = newY
	ball.setPosition([x,y])

def repositiony(ball, newX):
	# repositions y based on a known x
	x,y = ball.getPosition()
	dx, dy = ball.getVelocity()
	y = y - (dy/dx)*(x - newX)
	x = newX
	ball.setPosition([x,y])

def crossesLineHorizontal(ball, yLine, xLimits):
	# ball = ball object
	# yLine = y position of horizontal line
	# xLimits = [min, max]

	# y - y1 = m(x - x1)
	# y = mx + b
	x, y = ball.getPosition()
	x1, y1 = ball.getPrevious()

	if(y == y1 or x == x1):
		return False

	m = (y - y1)/(x - x1)
	b = y - m*x
	
	if (m == 0):
		return False
	else:
		xIntersect = (yLine - b)/m

	if (yLine > min(y, y1) and yLine < max(y, y1) and xIntersect > xLimits[0] and xIntersect < xLimits[1]):
		return True
	else:
		return False

def crossesLineVertical(ball, xLine, yLimits):
	x, y = ball.getPosition()
	x1, y1 = ball.getPrevious()

	if(x == x1 or y == y1):
		return False

	m = (y - y1)/(x - x1)
	b = y - m*x

	if (m == 0):
		yIntersect = y
	else:
		yIntersect = m*xLine + b

	if (xLine > min(x, x1) and xLine < max(x, x1) and yIntersect > yLimits[0] and yIntersect < yLimits[1]):
		return True
	else:
		return False

def checkBoundary(ball, canvas):
	dx, dy = ball.getVelocity()
	top = 0 + ball.getDimensions()
	bottom = canvas[1] - ball.getDimensions()

	if(crossesLineHorizontal(ball, top, [0, canvas[0]])):
		ball.setPosition(ball.getPrevious())
		repositionx(ball, top)
		dy = -1*dy	
		
	elif(crossesLineHorizontal(ball, bottom, [0, canvas[0]])):
		ball.setPosition(ball.getPrevious())
		repositionx(ball, bottom)
		dy = -1*dy

	ball.setVelocity([dx, dy])

def checkObstacle(ball, obstacle):
	hit = False
	if(obstacle.getType() > 0):
		x,y = obstacle.getPosition()
		width, height = obstacle.getDimensions()

		ballDX, ballDY = ball.getVelocity()
		ballRadius = ball.getDimensions()

		top = y - ballRadius
		bottom = y + height + ballRadius
		left = x - ballRadius
		right =  x + width + ballRadius

		if (crossesLineHorizontal(ball, top, [left, right])):
			#check top\
			ball.setPosition(ball.getPrevious())
			repositionx(ball, top)
			ballDY = -1*ballDY
			hit = True
		elif (crossesLineHorizontal(ball, bottom, [left, right])):
			#check bottom
			ball.setPosition(ball.getPrevious())
			repositionx(ball, bottom)
			ballDY = -1*ballDY
			hit = True
		elif(crossesLineVertical(ball, left, [top, bottom])):
			#check left
			ball.setPosition(ball.getPrevious())
			repositiony(ball, left)
			ballDX = -1*ballDX
			hit = True
		elif(crossesLineVertical(ball, right, [top, bottom])):
			#check right
			ball.setPosition(ball.getPrevious())		
			repositiony(ball, right)
			ballDX = -1*ballDX
			hit = True

		ball.setVelocity([ballDX, ballDY])

	elif(obstacle.getType() < 0):
		x1, y1 = ball.getPosition()
		x2, y2 = obstacle.getPosition()

		dx = x1 - x2
		dy = y1 - y2
		distance = sqrt(dx**2 + dy**2)

		r = ball.getDimensions() + obstacle.getDimensions()

		if (distance <= r):
			hit = True

			# reposition ball1 to be tangent to ball2
			x1prime, y1prime = ball1.getPrevious()
			m = (y1 - y1prime)/(x1 - x1prime)
			b = y1 - m*x1

			A = 1 + m**2
			B = -2*x2 + 2*m*b - 2*m*y2
			C = x2**2 + b**2 - 2*b*y2 + y2**2 - r**2

			newX1 = (-1*B + sqrt(B**2  - 4*A*C))/(2*A)
			newX2 = (-1*B - sqrt(B**2  - 4*A*C))/(2*A)

			if (abs(x1 - newX1) < abs(x1 - newX2)):
				x1 = newX1
			else:
				x1 = newX2

			y1 = m*x1 + b
			ball1.setPosition([x1, y1])

			# bounce ball
			angleA = ball1.getTheta()
			angleB = arctan((y1-y2)/(x1-x2))

			newTheta = 180 - 2*angleB - angleA
			ball1.setTheta(newTheta)

	if (hit):
			obstacle.switchHit()

	return hit
